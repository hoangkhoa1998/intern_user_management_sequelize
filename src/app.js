const express = require("express");
const app = express();
const PORT = process.env.PORT || 8080;
const handlebars = require("express-handlebars");
const morgan = require("morgan");
const path = require("path");
const router = require("./routes/index");

app.use(morgan("combined"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));
app.engine(
  ".hbs",
  handlebars.engine({
    extname: ".hbs",
  })
);
app.set("view engine", ".hbs");
app.set("views", path.join(__dirname, "views"));
app.use("/", router);

app.listen(PORT, () => {
  console.log("App is running on " + PORT);
});
