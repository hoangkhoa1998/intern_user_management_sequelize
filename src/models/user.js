const { Sequelize, DataTypes } = require("sequelize");
const db = require("../util/dbConfig");

module.exports = db.define("User", {
  userId: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    validate: {
      checkLength(value) {
        if (value.length < 4 && value.length > 10) {
          throw new Error("Username only between 4 - 10 in length");
        }
      },
    },
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    validate: {
      checkEmail(value) {
        const regexEmail =
          /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!regexEmail.test(value)) {
          throw new Error("Please type the right format of email !");
        }
      },
    },
  },
  phoneNumber: {
    type: DataTypes.INTEGER,
    allowNull: false,
    unique: true,
    validate: {
      checkVietNamPhoneNumber(value) {
        const regex =
          /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/;
        if (!regex.test(value)) {
          throw new Error("This is not Vietnam phone number");
        }
      },
    },
  },
});
