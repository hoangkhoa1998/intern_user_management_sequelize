const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

router.get("/", userController.allUsers);
router.get("/create-user", userController.createUser);
router.post("/create-user", userController.saveUserToDatabase);

module.exports = router;
