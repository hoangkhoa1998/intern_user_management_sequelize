const User = require("../models/user");

const allUsers = async (req, res) => {
  await res.render("home");
};

const createUser = async (req, res) => {
  await res.render("user/create");
};

const saveUserToDatabase = async (req, res) => {
  const { username, email, phoneNumber } = await req.body;
  const user = await User.create({
    username,
    email,
    phoneNumber,
  }).catch((error) => {
    console.log(error);
    console.log("Cannot connect to sv");
  });
  res.redirect("/");
  console.log("userData", user);
};

module.exports = { allUsers, createUser, saveUserToDatabase };
